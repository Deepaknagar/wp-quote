<?php
function wp_quotes_update() {
    global $wpdb;
    $table_name = $wpdb->prefix . "quotes";
    $id = $_GET["id"];
    $quote = $_POST["quotes"];
    $author_id = $_POST["author_id"];
//update
    if (isset($_POST['update'])) {
        $wpdb->update(
                $table_name, //table
                 array('quote' => $quote,'author_id' => $author_id), //data
               
                array('id' => $id), //where
                array('%s','%s'), //data format
                
                array('%s') //where format
        );
    }
//delete
    else if (isset($_POST['delete'])) {
        $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE id = %s", $id));
    } else {//selecting value to update	
        $quotes = $wpdb->get_results($wpdb->prepare("SELECT id,quote,author_id from $table_name where id=%s", $id));
        foreach ($quotes as $s) {
            $quote = $s->quote;
             $author_id = $s->author_id;
             
        }
    }


      $table_name_author = $wpdb->prefix . "author";
      $author = $wpdb->get_results("SELECT id,name from $table_name_author");

    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>wp-quotes/style-admin.css" rel="stylesheet" />
    <div class="wrap">
        <h2>Quotes</h2>

        <?php if ($_POST['delete']) { ?>
            <div class="updated"><p>Quote deleted</p></div>
            <a href="<?php echo admin_url('admin.php?page=wp_quotes_list') ?>">&laquo; Back to quote list</a>

        <?php } else if ($_POST['update']) { ?>
            <div class="updated"><p>Quote updated</p></div>
            <a href="<?php echo admin_url('admin.php?page=wp_quotes_list') ?>">&laquo; Back to quote list</a>

        <?php } else { ?>
            <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                <table class='wp-list-table widefat fixed'>
                  <tr>
                    <td><textarea  name="quotes" rows="4" cols="23"  required/>
                        <?php echo $quote; ?>
                    </textarea></td>
                    <td>
                        <select name="author_id" required>
    <?php

    foreach ($author as $auth) {
        # code...
   
    ?>
    <option value="<?php echo $auth->id?>"><?php echo $auth->name?></option>
   <?php
}
   ?>
</select>

                    </td>
                    </tr>
                </table>
                <input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
                <input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Are you sure want to delete?')">
            </form>
        <?php } ?>

    </div>
    <?php
}
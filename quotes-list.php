<?php
function wp_quotes_list() {
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/wp-quotes/style-admin.css" rel="stylesheet" />
    <div class="wrap">
        <h2>All Quotes</h2>
        <div class="tablenav top">
            <div class="alignleft actions">
                <a href="<?php echo admin_url('admin.php?page=wp_quotes_create'); ?>">Add New</a>
            </div>
            <br class="clear">
        </div>
        <?php
        global $wpdb;
        $table_name = $wpdb->prefix . "quotes";
        
        $rows = $wpdb->get_results("SELECT id,quote from $table_name");
        ?>
        <table class='wp-list-table widefat fixed striped posts'>
            <tr>
                <th class="manage-column ss-list-width">ID</th>
                <th class="manage-column ss-list-width">Name</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($rows as $row) { ?>
                <tr>
                    <td class="manage-column ss-list-width"><?php echo $row->id; ?></td>
                    <td class="manage-column ss-list-width"><?php echo $row->quote; ?></td>
                    <td><a href="<?php echo admin_url('admin.php?page=wp_quotes_update&id=' . $row->id); ?>">Update</a></td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php
}

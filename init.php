<?php
/*
Plugin Name: Wp Quotes
Description:
Version: 1
Author: Deepak nagar
Author URI: 
*/
// function to create the DB / Options / Defaults					
function wp_options_autor_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . "author";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(50) CHARACTER SET utf8 NOT NULL,
             `email` varchar(50) CHARACTER SET utf8 NOT NULL,
            PRIMARY KEY (`id`)
          ) $charset_collate; ";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
}
function wp_options_quotes_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . "quotes";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `quote` text  NOT NULL,
             `author_id` int(11)  NOT NULL,
            PRIMARY KEY (`id`)
          ) $charset_collate; ";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
}


add_action('wp_footer', 'display_quotes');
function display_quotes() {


    global $wpdb;
    $table_name = $wpdb->prefix . "quotes";
    // Shortcodes RETURN content, so store in a variable to return
    $content = '<table>';
    $content .= '</tr><th>Quotes</th><th>Quote</th><th>Author</th></tr>';
    global $wpdb;
        $table_name = $wpdb->prefix . "quotes";
        
        $rows = $wpdb->get_results("SELECT * from $table_name");
  
    foreach ( $rows AS $row ) {
        $content .= '<tr>';
        // Modify these to match the database structure
        $content .= '<td>' . $row->{'Quote State'} . '</td>';
        $content .= '<td>' . $row->quote . '</td>';
        $content .= '<td>' . $row->author_id . '</td>';
       
        $content .= '</tr>';
    }
    $content .= '</table>';
     echo $content;
}
// this function generates the shortcode output
function quote_shortcode( $args ) {

  

    // return the table
   
}
// run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'wp_options_autor_install');
register_activation_hook(__FILE__, 'wp_options_quotes_install');
//menu items
add_action('admin_menu','wp_quotes_menu');
function wp_quotes_menu() {
	
	//this is the main item for the menu
	

	add_menu_page('Author', //page title
	'Author', //menu title
	'manage_options', //capabilities
	'wp_author_list', //menu slug
	'wp_author_list', //function
	'dashicons-admin-users'

	);
	
	
	//this is a submenu
	add_submenu_page('wp_author_list', //parent slug
	'Add New Author', //page title
	'Add New', //menu title
	'manage_options', //capability
	'wp_author_create', //menu slug
	'wp_author_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update Author', //page title
	'Update', //menu title
	'manage_options', //capability
	'wp_author_update', //menu slug
	'wp_author_update'); //function


	add_menu_page('Quotes', //page title
	'Quotes', //menu title
	'manage_options', //capabilities
	'wp_quotes_list', //menu slug
	'wp_quotes_list', //function
	'dashicons-format-aside'

	);
	add_submenu_page('wp_quotes_list', //parent slug
	'Add New Quotes', //page title
	'Add New', //menu title
	'manage_options', //capability
	'wp_quotes_create', //menu slug
	'wp_quotes_create'); //function

	add_submenu_page(null, //parent slug
	'Update Quotes', //page title
	'Update', //menu title
	'manage_options', //capability
	'wp_quotes_update', //menu slug
	'wp_quotes_update'); //function

}
define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'quotes-list.php');
require_once(ROOTDIR . 'quotes-create.php');
require_once(ROOTDIR . 'quotes-update.php');
require_once(ROOTDIR . 'author-list.php');
require_once(ROOTDIR . 'author-create.php');
require_once(ROOTDIR . 'author-update.php');